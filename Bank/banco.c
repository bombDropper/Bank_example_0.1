#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {

    const int CLAVE = 1230;
    const int  SALIR = 0;
    const int ACCEDER_A_CUENTA = 1;
    int fondos = 0;
    int monto = 0;
    int opcionIngresada = 0;

    do {
        
        system("clear");
        printf("1   Ingresar a mi cuenta \n");
        printf("0   Salir \n\n");

        scanf("%d", &opcionIngresada);
        system("clear");

        if(opcionIngresada == ACCEDER_A_CUENTA){

            printf("Mi clave es ");
            scanf("%d", &opcionIngresada);

            if(opcionIngresada == CLAVE){

                do{

                    system("clear");
                    printf("1   Cuanto dinero tengo? \n");
                    printf("2   Quiero retirar dinero \n");
                    printf("3   Quiero guardar dinero  \n");
                    printf("0   Salir \n\n");

                    scanf("%d", &opcionIngresada);

                    switch(opcionIngresada){

                        case 1:
                            if(fondos == 0){

                                printf("No tienes fondos en tu cuenta. \n");
                                system("sleep 2s");
                                system("clear");

                            } else {

                                printf("Tienes %d pesos. \n");
                                system("sleep 2s");
                                system("clear");

                            }

                            break;

                        case 2:
                            if(fondos == 0){

                                printf("No tienes fondos en tu cuenta. \n");
                                system("sleep 2s");
                                system("clear");

                            } else {

                                do {

                                    printf("Quiero retirar ");
                                    scanf("%d", &monto);
                                    printf("\n");
                                    system("clear");

                                    if(monto > fondos){

                                        printf("Solo tienes %d pesos! \n");
                                        system("sleep 2s");
                                        system("clear");

                                    } else {

                                        printf("Resumen de la operación: \n\n");
                                        printf("Retire: %d pesos. \n", monto);
                                        printf("Me quedan: %d pesos.\n", fondos);
                                        system("sleep 4s");
                                        system("clear");

                                    }

                                }while(monto > fondos);

                            }

                            break;

                        case 3:
                            if(monto < 0){

                                printf("Tienes %d pesos. \n");
                                system("sleep 2s");
                                system("clear");

                            }
                        case 0:
                            printf("Gracias\n");
                            system("sleep 2s");
                            continue;

                        default:
                            system("clear");
                            printf("Opcion invalida! \n");
                            system("sleep 2s");
                            system("clear");
                            break;

                    }

                }while(opcionIngresada != SALIR);

            } else {

                printf("Clave incorrecta\n");
                system("sleep 3s");
                system("clear");

            }
        }

        system("clear");

    } while(opcionIngresada != SALIR);

    return 0;
}
